from django.views.generic import TemplateView
from django.conf import settings
import os, re


class IndexView(TemplateView):
    template_name = 'landing/index_ru.html'

    def get(self, request, *args, **kwargs):
        lang = 'ru'
        if 'en' in request.GET.dict():
            lang = 'en'
            self.template_name = 'landing/index_en.html'
        ctx = self.get_context_data()
        ctx['lang'] = lang
        return self.render_to_response(ctx)


class DownloadView(TemplateView):
    template_name = 'landing/download.html'

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data()
        data = {}
        for OS in ['win', 'lin', 'mac']:
            data[OS] = get_release(OS)
        ctx['downloads'] = data
        return self.render_to_response(ctx)


def get_release(OS):
    d = os.path.join(settings.DOWNLOAD_ROOT, OS)
    print(d)
    if not os.path.exists(d):
        print('Path not exists:', d)
        return
    # releases = sorted(os.listdir(d))
    releases = sorted(os.listdir(d), key=lambda f: [int(x) for x in re.findall(r"\d+", f)])
    if releases:
        return dict(
            name=releases[-1],
            url='/media/downloads/{}/{}'.format(OS, releases[-1])
        )
