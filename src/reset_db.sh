#!/usr/bin/env bash
export DJANGO_SETTINGS_MODULE="main.settings"
./manage.py reset_db -v 3 --settings ${DJANGO_SETTINGS_MODULE} --noinput -c
apps=$(python ./app_list.py)
./manage.py makemigrations ${apps}
./manage.py migrate

if [ $full_path="" ]; then
    echo "error"
fi
