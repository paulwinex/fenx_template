from django.db import models
from custom_user.models import EmailUser as User
from django.utils.translation import gettext as _


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    display_name = models.CharField(_('display name'), max_length=64)
    site = models.URLField(_('site'), max_length=256)
