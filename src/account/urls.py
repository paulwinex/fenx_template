from django.urls import path
from .views import (LoginView, RegisterView, ProfileView, LogoutView, RestorePasswordView, SaveProfileView,
                    SetPasswordView, ActivateView)
from django.views.decorators.csrf import csrf_exempt

app_name = 'account'
urlpatterns = [
    path('', ProfileView.as_view(), name='profile'),
    path('register/', RegisterView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('restore/', RestorePasswordView.as_view(), name='restore'),
    path('save_profile/', SaveProfileView.as_view(), name='save'),
    path('set_password/', SetPasswordView.as_view(), name='set_password'),
    path('activate/', ActivateView.as_view(), name='activate'),
]
