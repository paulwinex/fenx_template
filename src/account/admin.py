from django.contrib import admin
from .models import Profile


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'user',
                'display_name',
                'site',
            )},),
    )
    list_display = ('user', 'display_name')
    search_fields = ('user__email', 'display_name',)
