from django.views.generic import TemplateView, RedirectView, View
from django.shortcuts import reverse
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.conf import settings


class IndexView(TemplateView):
    template_name = 'base.html'


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'account/profile.html'


class LoginView(TemplateView):
    template_name = 'account/login.html'

    def post(self, request):
        username = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return JsonResponse(dict(
                success=True,
                url=request.GET.get('next') or settings.LOGIN_REDIRECT_URL
            ))
        else:
            return JsonResponse(dict(
                success=False,
                error='Wrong email or password'
            ))


class LogoutView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        return reverse('account:login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class RegisterView(TemplateView):
    template_name = 'account/register.html'

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data()
        ctx['enabled'] = settings.ALLOW_REGISTER
        return self.render_to_response(ctx)


class RestorePasswordView(TemplateView):
    template_name = 'account/restore.html'

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data()
        ctx['enabled'] = settings.EMAIL_IS_ACTIVE
        return self.render_to_response(ctx)


class SaveProfileView(LoginRequiredMixin, View):
    def post(self, request):
        print(request.POST.dict())
        return JsonResponse(dict(
            success=True
        ))


class SetPasswordView(LoginRequiredMixin, View):
    pass


class ActivateView(TemplateView):
    template_name = 'account/activated.html'






