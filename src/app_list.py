import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', "main.settings")
import django
django.setup()
from django.conf import settings
print(' '.join(settings.PROJECT_APPS))
