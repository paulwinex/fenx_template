import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_ROOT = os.path.dirname(BASE_DIR)
SECRET_KEY = '1y#shjw=0v21xjqwrtmad+fqf#rd=rwe9r3245tgrek2+)'
DEBUG = False
ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'jet',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'easy_thumbnails',
    'django_extensions',
]
PROJECT_APPS = [
    'account',
    'custom_user'
]
INSTALLED_APPS.extend(PROJECT_APPS)
AUTH_USER_MODEL = 'custom_user.EmailUser'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'main.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
            os.path.join(BASE_DIR, 'account', 'templates'),
            os.path.join(BASE_DIR, 'landing', 'templates'),
        ]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
            ],
        },
    },
]

WSGI_APPLICATION = 'main.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
# LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'ru'

LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
)
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LOCALE_PATHS = (
    # 'locale',
    os.path.join(BASE_DIR, 'locale'),
)

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'data', 'static')
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'data', 'media')
DOWNLOAD_ROOT = os.path.join(MEDIA_ROOT, 'downloads')


STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
    os.path.join(BASE_DIR, 'landing', "static")
]

THUMBNAIL_ALIASES = {
    '': {
        'module': {'size': (128, 128), 'crop': True},
    }
}

MODULES_PER_PAGE = 20
LOGOUT_REDIRECT_URL = '/account/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/account/login/'
ALLOW_REGISTER = False
ALLOW_REGISTER_MODULES = False

ADMIN_EMAIL = 'fenxpipeline@gmail.com'
EMAIL_IS_ACTIVE = False


if os.getenv('IS_DOCKER_CONTAINER'):
    try:
        from .docker_settings import *
    except ImportError:
        pass
else:
    try:
        from .local_settings import *
    except ImportError:
        pass
