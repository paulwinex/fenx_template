import os

SECRET_KEY = 'asdjkKJSfsdDAaaWEWA*%&*L89&*%SD&*ua90'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('POSTGRES_DB', ''),
        'USER': os.getenv('POSTGRES_USER', ''),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', ''),
        'HOST': os.getenv('POSTGRES_SERVICE', 'localhost'),
        'PORT': os.getenv('DATABASE_PORT', 5432),
    }
}
# DEBUG = False
STATIC_ROOT = '/data/static'
MEDIA_ROOT = '/data/media'
LOG_DIR = '/data/logs'
BACKUP_DIR = '/data/backup'
DOWNLOAD_ROOT = os.path.join(MEDIA_ROOT, 'downloads')

