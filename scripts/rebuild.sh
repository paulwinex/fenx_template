#!/usr/bin/env bash

docker-compose down
sudo chmod -R 0755 ./data/*
docker rm $(docker ps -a -q -f status=exited)
docker-compose build
docker rmi $(docker images -f "dangling=true" -q)
