#!/usr/bin/env bash
# install docker
if [ ! -e "$(which docker)" ] ; then
    echo "Install Docker"
    sudo apt-get install curl
    sudo curl -sSL https://get.docker.com/ | sh
    sudo usermod -aG docker $(whoami)
    sudo apt-get -y install python-pip
    sudo pip install docker-compose

fi
# create local env file
file=".env_local"

if [ ! -e "$file" ] ; then
    echo "Create local env file"
    touch "$file"
fi
sudo systemctl stop nginx.service
docker-compose up -d
