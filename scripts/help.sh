#!/usr/bin/env bash

#Init dtabase
sudo su postgres
psql
CREATE DATABASE fenx_db WITH OWNER = "admin"  ENCODING = 'UTF8'; 
# DROP DATABASE advshop_db;
# create user
CREATE USER admin WITH PASSWORD 'xxxxxxxxx';
ALTER ROLE admin SET client_encoding TO 'utf8';
ALTER ROLE admin SET default_transaction_isolation TO 'read committed'; 
ALTER ROLE admin SET timezone TO 'UTC'; 
ALTER USER admin WITH CREATEDB;
# assign db to user
GRANT ALL PRIVILEGES ON DATABASE fenx_db TO admin; 
# list
\l
# user list
select usename from pg_user;
# выход из psql
\q
exit



### Show tables

docker exec -it database bash
psql -U postgres -d fenxdb
fenxdb=# \d