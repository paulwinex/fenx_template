FROM python:3.6
RUN apt-get update &&\
    apt-get install -yqq mc cron zip git net-tools wget
RUN mkdir -p /data/{fenx,logs,media,static,backup} &&\
    mkdir -p /data/download/{lin,win,mac} &&\
    mkdir -p /data/backup
ADD ./src/requirements.txt /data/fenx/requirements.txt
RUN pip install -r /data/fenx/requirements.txt
COPY ./src /data/fenx/
RUN touch /data/fenx/main/local_settings.py
WORKDIR /data/fenx/
EXPOSE 8000
ENTRYPOINT /data/fenx/start.sh
